package com.ingroupe.healthdocconvertor;

import com.ingroupe.healthdocconvertor.connector.dgcissuance.DgcIssuanceConnector;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients(basePackageClasses = DgcIssuanceConnector.class)
public class HealthDocumentConvertorApplication {

  public static void main(String[] args) {
    SpringApplication.run(HealthDocumentConvertorApplication.class, args);
  }

}
