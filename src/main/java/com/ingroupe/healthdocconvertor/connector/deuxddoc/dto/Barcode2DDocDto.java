package com.ingroupe.healthdocconvertor.connector.deuxddoc.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Barcode2DDocDto {
    private ZoneDto header;

    private ZoneDto message;

    private ZoneDto annexe;

    private Boolean isBlacklisted;

    private Signature signature;
}
