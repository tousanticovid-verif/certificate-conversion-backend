package com.ingroupe.healthdocconvertor.convertor.impl;

import com.ingroupe.healthdocconvertor.connector.deuxddoc.dto.AttestationType;
import com.ingroupe.healthdocconvertor.connector.deuxddoc.dto.DocumentDto;
import com.ingroupe.healthdocconvertor.connector.deuxddoc.dto.TwoDDocHealthCertResponseFacade;
import com.ingroupe.healthdocconvertor.exception.CannotParse2DDocException;
import com.ingroupe.healthdocconvertor.exception.Invalid2DDocInputValueException;
import ehn.techiop.hcert.kotlin.data.GreenCertificate;

import java.time.*;
import java.time.format.DateTimeFormatter;

public class GreenCertificateRequestAdapter {

    private static final String COVID19_DISEASE_AGENT_TARGETED_ID = "840539006";
    private static final String DETECTED_TEST_RESULT_CODE = "260373001";
    private static final String NOT_DETECTED_TEST_RESULT_CODE = "260415000";
    private static final String COVID19_VACCINE_CODE = "J07BX03";
    private final TwoDDocHealthCertResponseFacade twoDDocFacade;
    private GreenCertificateBuilder builder;

    public GreenCertificateRequestAdapter(DocumentDto twoDDoc) {
        validateArguments(twoDDoc);
        twoDDocFacade = new TwoDDocHealthCertResponseFacade(twoDDoc);
    }

    private void validateArguments(DocumentDto twoDDoc) {
        if (twoDDoc == null)
            throw new CannotParse2DDocException("The value of the 2DDoc parameter is null");
    }

    public GreenCertificate createEudgcRequest() {
        builder = new GreenCertificateBuilder();
        populateCommonData();

        AttestationType attestationType = twoDDocFacade.getAttestationType();
        switch (attestationType) {
            case TEST:
                if (twoDDocFacade.isAntiBodyTestType())
                    throw new Invalid2DDocInputValueException("Antibody test type is not supported");
                else
                    buildTestAttestation();
                break;
            case RECOVERY:
                buildRecoveryAttestation();
                break;
            case VACCINATION:
                buildVaccinationAttestation();
                break;
            default:
                throw new CannotParse2DDocException( "The type of 2DDoc is not supported" );

        }

        return builder.build();
    }

    private void populateCommonData() {
        builder.withSchemaVersion("1.3.0");
    }

    private void buildTestAttestation() {

        DateOfBirth dateOfBirth = new DateOfBirth(twoDDocFacade.getTestDateOfBirth());
        LocalDateTime date = parseDateTime(twoDDocFacade.getTestDate(), "dd/MM/yyyy HH:mm");
        final ZoneId zone = ZoneId.of("Europe/Paris");
        ZoneOffset zoneOffSet = zone.getRules().getOffset(date);
        OffsetDateTime offsetDateTime = date.atOffset(zoneOffSet);
        TestType testType = TestType.forTwoDDocType(twoDDocFacade.getTestType());

        builder
            .withSubject()
                .withFamilyName(twoDDocFacade.getTestPatientLastName())
                .withFamilyNameTransliterated(new NameStandardizer(twoDDocFacade.getTestPatientLastName()).standardize())
                .withGivenName(twoDDocFacade.getTestPatientFirstName())
                .withGivenNameTransliterated(new NameStandardizer(twoDDocFacade.getTestPatientFirstName()).standardize())
                .build()
            .withDateOfBirth(dateOfBirth.format())
            .addTest()
                .withCountry("FR")
                .withCertificateIssuer("DGS")
                .withDateTimeSample(offsetDateTime)
                .withTestFacility("France")
                .withTarget(COVID19_DISEASE_AGENT_TARGETED_ID)
                .withResultPositive(twoDDocFacade.isTestPositive() ? DETECTED_TEST_RESULT_CODE : NOT_DETECTED_TEST_RESULT_CODE)
                .withType(testType.getDgcType())
                .build();
    }

    private void buildRecoveryAttestation() {
        DateOfBirth dateOfBirth = new DateOfBirth(twoDDocFacade.getTestDateOfBirth());
        LocalDateTime date = parseDateTime(twoDDocFacade.getTestDate(), "dd/MM/yyyy HH:mm");
        LocalDateTime validityDateStart = date.plusDays(11);
        LocalDateTime validityDateEnd = date.plusDays(180);
        builder.
            withSubject()
                .withFamilyName(twoDDocFacade.getTestPatientLastName())
                .withFamilyNameTransliterated(new NameStandardizer(twoDDocFacade.getTestPatientLastName()).standardize())
                .withGivenName(twoDDocFacade.getTestPatientFirstName())
                .withGivenNameTransliterated(new NameStandardizer(twoDDocFacade.getTestPatientFirstName()).standardize())
                .build()
            .withDateOfBirth(dateOfBirth.format())
            .addRecoveryStatement()
                .withTarget(COVID19_DISEASE_AGENT_TARGETED_ID)
                .withDateOfFirstPositiveTestResult(date.toLocalDate())
                .withCountry("FR")
                .withCertificateIssuer("DGS")
                .withCertificateValidFrom(validityDateStart.toLocalDate())
                .withCertificateValidUntil(validityDateEnd.toLocalDate())
                .build();
    }

    private LocalDateTime parseDateTime(String dateString, String pattern) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        return LocalDateTime.parse(dateString, formatter);
    }

    private LocalDate parseDate(String dateString, String pattern) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        return LocalDate.parse(dateString, formatter);
    }

    private void buildVaccinationAttestation() {
        DateOfBirth dateOfBirth = new DateOfBirth(twoDDocFacade.getVaccineDateOfBirth());
        LocalDate vaccinationDate = parseDate(twoDDocFacade.getVaccinationDate(), "dd/MM/yyyy");
        VaccineType vaccineType = VaccineType.forTwoDDocName(twoDDocFacade.getVaccineName());

        builder
            .withSubject()
                .withFamilyName(twoDDocFacade.getVaccinePatientLastName())
                .withFamilyNameTransliterated(new NameStandardizer(twoDDocFacade.getVaccinePatientLastName()).standardize())
                .withGivenName(twoDDocFacade.getVaccinePatientFirstName())
                .withGivenNameTransliterated(new NameStandardizer(twoDDocFacade.getVaccinePatientFirstName()).standardize())
                .build()
            .withDateOfBirth(dateOfBirth.format())
            .addVaccination()
                .withTarget(COVID19_DISEASE_AGENT_TARGETED_ID)
                .withVaccine(COVID19_VACCINE_CODE)
                .withMedicinalProduct(vaccineType.getDgcName())
                .withAuthorizationHolder(vaccineType.getManufacturerName())
                .withDoseNumber(Integer.parseInt(twoDDocFacade.getVaccineDoseNumber()))
                .withDoseTotalNumber(Integer.parseInt(twoDDocFacade.getVaccineTotalDoses()))
                .withDate(vaccinationDate)
                .withCountry("FR")
                .withCertificateIssuer("DGS")
                .build();
    }

}
