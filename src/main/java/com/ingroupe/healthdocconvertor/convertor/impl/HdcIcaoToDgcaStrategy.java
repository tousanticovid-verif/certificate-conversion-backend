package com.ingroupe.healthdocconvertor.convertor.impl;

import com.ingroupe.healthdocconvertor.convertor.HdcStrategy;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class HdcIcaoToDgcaStrategy implements HdcStrategy {

    @Override
    public String decodeDocument(String chainEncoded) {
        log.info(" Strategy running {}", HdcIcaoToDgcaStrategy.class.getSimpleName());

        return "OK";
    }

}
