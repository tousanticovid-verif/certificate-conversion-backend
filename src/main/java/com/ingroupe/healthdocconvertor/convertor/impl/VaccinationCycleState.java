package com.ingroupe.healthdocconvertor.convertor.impl;

import com.ingroupe.healthdocconvertor.exception.Invalid2DDocInputValueException;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;

public enum VaccinationCycleState {

    INCOMPLETE("En cours"),
    COMPLETE("Terminé");

    private final String twoDDocState;

    VaccinationCycleState(String twoDDocState) {
        this.twoDDocState = twoDDocState;
    }

    public static VaccinationCycleState forTwoDDocState(String state) {
        return Arrays.stream(VaccinationCycleState.values())
            .filter(v -> StringUtils.equals(state, v.getTwoDDocState()))
            .findFirst()
            .orElseThrow(() ->
                new Invalid2DDocInputValueException("No corresponding type has been found for the vaccine cycle state"));
    }

    public String getTwoDDocState() {
        return twoDDocState;
    }
}
