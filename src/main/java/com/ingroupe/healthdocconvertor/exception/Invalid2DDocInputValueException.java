package com.ingroupe.healthdocconvertor.exception;


import com.ingroupe.healthdocconvertor.convertor.impl.VaccinationCycleState;

public class Invalid2DDocInputValueException extends RuntimeException {

    private VaccinationCycleState vaccinationCycleState;
    private int injectedDoseNumber;
    private int completeVaccinationCycleDoseNumber;


    public Invalid2DDocInputValueException(String message) {
            super(message);
        }


    public Invalid2DDocInputValueException(VaccinationCycleState vaccinationCycleState, int injectedDoseNumber,
                                                 int completeVaccinationCycleDoseNumber, String message) {
        super(message);
        this.injectedDoseNumber = injectedDoseNumber;
        this.completeVaccinationCycleDoseNumber = completeVaccinationCycleDoseNumber;
        this.vaccinationCycleState = vaccinationCycleState;
    }


    public VaccinationCycleState getVaccinationCycleState() {
        return vaccinationCycleState;
    }

    public int getInjectedDoseNumber() {
        return injectedDoseNumber;
    }

    public int getCompleteVaccinationCycleDoseNumber() {
        return completeVaccinationCycleDoseNumber;
    }


}
