package com.ingroupe.healthdocconvertor.helper;

import com.ingroupe.healthdocconvertor.dto.DocumentRequest;

public interface HealthDocumentConvertorHelper {

    String genereteKey(DocumentRequest.DocumentType source, DocumentRequest.DocumentType destination);

}
