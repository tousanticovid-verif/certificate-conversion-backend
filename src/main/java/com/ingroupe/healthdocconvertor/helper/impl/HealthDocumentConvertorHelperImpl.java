package com.ingroupe.healthdocconvertor.helper.impl;

import com.ingroupe.healthdocconvertor.constant.HealthDocumentConvertorConstant;
import com.ingroupe.healthdocconvertor.dto.DocumentRequest.DocumentType;
import com.ingroupe.healthdocconvertor.helper.HealthDocumentConvertorHelper;

import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Component;

@Component
@Slf4j
public class HealthDocumentConvertorHelperImpl implements HealthDocumentConvertorHelper {

    @Override
    public String genereteKey(DocumentType source, DocumentType destination) {
        log.debug("genereteKey source = {} , destination = {}", source, destination);

        StringBuilder sb = new StringBuilder(source.name());
        sb.append(HealthDocumentConvertorConstant.CONVERSION_SEPARATOR);
        sb.append(destination.name());
        return sb.toString();
    }

}
