package com.ingroupe.healthdocconvertor.convertor.impl;

import com.ingroupe.healthdocconvertor.exception.CannotParse2DDocException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class DateOfBirthTest {

    @Test
    void givenNull_shouldThrowException() {
        assertThrowsException(null);
    }

    @Test
    void givenEmptyString_shouldThrowException() {
        assertThrowsException("");
    }

    @Test
    void givenNotEvenADate_shouldThrowException() {
        assertThrowsException("foobar");
    }

    @Test
    void givenMissingDayOrMonth_shouldThrowException() {
        assertThrowsException("10/2015");
        assertThrowsException("/10/2015");
        assertThrowsException("21//2015");
    }

    @Test
    void givenMissingYear_shouldThrowException() {
        assertThrowsException("21/10");
        assertThrowsException("21/10/");
    }

    @Test
    void givenOnlyYear_shouldThrowException() {
        assertThrowsException("1998");
        assertThrowsException("/1998");
        assertThrowsException("//1998");
    }

    @Test
    void givenDateWithDashesAsSeparators_shouldThrowException() {
        assertThrowsException("21-10-2015");
    }

    @Test
    void givenNegativeDay_shouldThrowException() {
        assertThrowsException("-21/10/2015");
    }

    @Test
    void givenNegativeMonth_shouldThrowException() {
        assertThrowsException("21/-10/2015");
    }

    @Test
    void givenNegativeYear_shouldThrowException() {
        assertThrowsException("21/10/-2015");
    }

    @Test
    void givenDateWithTextBefore_shouldThrowException() {
        assertThrowsException("foo21/10/2015");
    }

    @Test
    void givenDateWithTextAfter_shouldThrowException() {
        assertThrowsException("21/10/2015foo");
    }

    @Test
    void givenDateWithTextBeforeAndAfter_shouldThrowException() {
        assertThrowsException("foo21/10/2015bar");
    }

    @Test
    void givenRegularDateNotPrependedWith0_shouldThrowException() {
        assertThrowsException("1/2/2015");
    }

    @Test
    void givenRegularDate_shouldParseDate() {
        assertDate("21/10/2015", "2015-10-21");
    }

    @Test
    void givenRegularDatePrependedWith0_shouldParseDate() {
        assertDate("01/02/2015", "2015-02-01");
    }

    @Test
    void givenLunarDateWithDayAs00_shouldParseDate() {
        assertDate("00/10/2015", "2015-10");
    }

    @Test
    void givenLunarDateWithDayGreaterThan31_shouldParseDate() {
        assertDate("32/10/2015", "2015-10");
    }

    @Test
    void givenLunarDateWithDayGreaterThan30ForAMonthWith30Days_shouldParseDate() {
        assertDate("31/04/2015", "2015-04");
    }

    @Test
    void givenLunarDateWithMonthAs00_shouldParseDate() {
        assertDate("21/00/2015", "2015");
    }

    @Test
    void givenLunarDateWithMonthGreaterThan12_shouldParseDate() {
        assertDate("21/13/2015",  "2015");
    }

    private void assertThrowsException(String dateOfBirthString) {
        CannotParse2DDocException exception =
                assertThrows(CannotParse2DDocException.class, () -> new DateOfBirth(dateOfBirthString));
        assertEquals(String.format("The date of birth is invalid : [%s]",dateOfBirthString), exception.getMessage());
    }

    private void assertDate(String dateOfBirthString, String expectedFormattedDate) {
        DateOfBirth date = new DateOfBirth(dateOfBirthString);

        assertEquals(expectedFormattedDate, date.format());
    }

}
