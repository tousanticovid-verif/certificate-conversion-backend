package com.ingroupe.healthdocconvertor.convertor.impl;

import ehn.techiop.hcert.kotlin.data.GreenCertificate;
import ehn.techiop.hcert.kotlin.data.Person;
import ehn.techiop.hcert.kotlin.data.RecoveryStatement;
import ehn.techiop.hcert.kotlin.data.Vaccination;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Objects;

import static com.ingroupe.healthdocconvertor.util.DateUtils.javaLocalDateOf;
import static com.ingroupe.healthdocconvertor.util.DateUtils.javaLocalDateTimeOf;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class GreenCertificateBuilderTest {

    private static final String COVID19_DISEASE_AGENT_TARGETED_ID = "840539006";
    private static final String DETECTED_TEST_RESULT_CODE = "260373001";
    private static final String NOT_DETECTED_TEST_RESULT_CODE = "260415000";
    private static final String COVID19_VACCINE_CODE = "J07BX03";

    @Test
    void shouldBuildGreenCertificate() {
        GreenCertificateBuilder builder = getBuilder();
        GreenCertificate greenCertificate = builder.build();

        assertEquals("1.3.0", greenCertificate.getSchemaVersion());
        assertEquals("1985-10-21", greenCertificate.getDateOfBirthString());
        assertSubject(greenCertificate.getSubject());
        assertVaccinations(Objects.requireNonNull(greenCertificate.getVaccinations()));
        assertRecoveryStatements(Objects.requireNonNull(greenCertificate.getRecoveryStatements()));
        assertTests(Objects.requireNonNull(greenCertificate.getTests()));
    }

    @NotNull
    private GreenCertificateBuilder getBuilder() {
        GreenCertificateBuilder builder = new GreenCertificateBuilder();

        builder.withSchemaVersion("1.3.0")
            .withSubject()
                .withGivenName("Elie")
                .withGivenNameTransliterated("ELIE")
                .withFamilyName("Coptaire")
                .withFamilyNameTransliterated("COPTAIRE")
                .build()
            .withDateOfBirth("1985-10-21")
            .addVaccination()
                .withTarget(COVID19_DISEASE_AGENT_TARGETED_ID)
                .withVaccine(COVID19_VACCINE_CODE)
                .withMedicinalProduct("EU/1/20/1507")
                .withAuthorizationHolder("ORG-100031184")
                .withDoseNumber(1)
                .withDoseTotalNumber(2)
                .withDate(LocalDate.of(2015, 10, 19))
                .withCountry("country42")
                .withCertificateIssuer("certificateIssuer42")
                .withCertificateIdentifier("certificateIdentifier42")
                .build()
            .addVaccination()
                .withTarget(COVID19_DISEASE_AGENT_TARGETED_ID)
                .withVaccine("1119349007")
                .withMedicinalProduct("EU/1/20/1528")
                .withAuthorizationHolder("ORG-100001699")
                .withDoseNumber(3)
                .withDoseTotalNumber(4)
                .withDate(LocalDate.of(2015, 10, 20))
                .withCountry("country43")
                .withCertificateIssuer("certificateIssuer43")
                .withCertificateIdentifier("certificateIdentifier43")
                .build()
            .addRecoveryStatement()
                .withTarget(COVID19_DISEASE_AGENT_TARGETED_ID)
                .withDateOfFirstPositiveTestResult(LocalDate.of(2015, 10, 21))
                .withCountry("country42")
                .withCertificateIssuer("certificateIssuer42")
                .withCertificateValidFrom(LocalDate.of(2015, 10, 22))
                .withCertificateValidUntil(LocalDate.of(2015, 10, 23))
                .withCertificateIdentifier("certIdentifier42")
                .build()
            .addRecoveryStatement()
                .withTarget(COVID19_DISEASE_AGENT_TARGETED_ID)
                .withDateOfFirstPositiveTestResult(LocalDate.of(2015, 10, 24))
                .withCountry("country43")
                .withCertificateIssuer("certificateIssuer43")
                .withCertificateValidFrom(LocalDate.of(2015, 10, 25))
                .withCertificateValidUntil(LocalDate.of(2015, 10, 26))
                .withCertificateIdentifier("certIdentifier43")
                .build()
            .addTest()
                .withTarget(COVID19_DISEASE_AGENT_TARGETED_ID)
                .withType("LP6464-4")
                .withNameNaa("nameNaa42")
                .withNameRat("1232")
                .withDateTimeSample(OffsetDateTime.of(LocalDateTime.of(2015, 10, 27, 12, 34, 56), ZoneOffset.UTC))
                .withResultPositive(NOT_DETECTED_TEST_RESULT_CODE)
                .withTestFacility("testFacility42")
                .withCountry("country42")
                .withCertificateIssuer("certificateIssuer42")
                .withCertificateIdentifier("certificateIdentifier42")
                .build()
            .addTest()
                .withTarget(COVID19_DISEASE_AGENT_TARGETED_ID)
                .withType("LP217198-3")
                .withNameNaa("nameNaa43")
                .withNameRat("1304")
                .withDateTimeSample(OffsetDateTime.of(LocalDateTime.of(2015, 10, 27, 12, 34, 58), ZoneOffset.UTC))
                .withResultPositive(DETECTED_TEST_RESULT_CODE)
                .withTestFacility("testFacility43")
                .withCountry("country43")
                .withCertificateIssuer("certificateIssuer43")
                .withCertificateIdentifier("certificateIdentifier43")
                .build();
        return builder;
    }

    private void assertSubject(Person subject) {
        assertEquals("Elie", subject.getGivenName());
        assertEquals("ELIE", subject.getGivenNameTransliterated());
        assertEquals("Coptaire", subject.getFamilyName());
        assertEquals("COPTAIRE", subject.getFamilyNameTransliterated());
    }

    private void assertVaccinations(Vaccination[] vaccinations) {
        assertEquals(2, vaccinations.length);

        Vaccination firstVaccination = vaccinations[0];
        assertEquals("COVID-19", firstVaccination.getTarget().getValueSetEntry().getDisplay());
        assertEquals("covid-19 vaccines", firstVaccination.getVaccine().getValueSetEntry().getDisplay());
        assertEquals("COVID-19 Vaccine Moderna", firstVaccination.getMedicinalProduct().getValueSetEntry().getDisplay());
        assertEquals("Moderna Biotech Spain S.L.", firstVaccination.getAuthorizationHolder().getValueSetEntry().getDisplay());
        assertEquals(1, firstVaccination.getDoseNumber());
        assertEquals(2, firstVaccination.getDoseTotalNumber());
        assertEquals(LocalDate.of(2015, 10, 19), javaLocalDateOf(firstVaccination.getDate()));
        assertEquals("country42", firstVaccination.getCountry());
        assertEquals("certificateIssuer42", firstVaccination.getCertificateIssuer());
        assertEquals("certificateIdentifier42", firstVaccination.getCertificateIdentifier());

        Vaccination secondVaccination = vaccinations[1];
        assertEquals("COVID-19", secondVaccination.getTarget().getValueSetEntry().getDisplay());
        assertEquals("SARS-CoV-2 mRNA vaccine", secondVaccination.getVaccine().getValueSetEntry().getDisplay());
        assertEquals("Comirnaty", secondVaccination.getMedicinalProduct().getValueSetEntry().getDisplay());
        assertEquals("AstraZeneca AB", secondVaccination.getAuthorizationHolder().getValueSetEntry().getDisplay());
        assertEquals(3, secondVaccination.getDoseNumber());
        assertEquals(4, secondVaccination.getDoseTotalNumber());
        assertEquals(LocalDate.of(2015, 10, 20), javaLocalDateOf(secondVaccination.getDate()));
        assertEquals("country43", secondVaccination.getCountry());
        assertEquals("certificateIssuer43", secondVaccination.getCertificateIssuer());
        assertEquals("certificateIdentifier43", secondVaccination.getCertificateIdentifier());
    }

    private void assertRecoveryStatements(RecoveryStatement[] recoveryStatements) {
        assertEquals(2, recoveryStatements.length);

        RecoveryStatement firstRecovery = recoveryStatements[0];
        assertEquals("COVID-19", firstRecovery.getTarget().getValueSetEntry().getDisplay());
        assertEquals(LocalDate.of(2015, 10, 21), javaLocalDateOf(firstRecovery.getDateOfFirstPositiveTestResult()));
        assertEquals("country42", firstRecovery.getCountry());
        assertEquals("certificateIssuer42", firstRecovery.getCertificateIssuer());
        assertEquals(LocalDate.of(2015, 10, 22), javaLocalDateOf(firstRecovery.getCertificateValidFrom()));
        assertEquals(LocalDate.of(2015, 10, 23), javaLocalDateOf(firstRecovery.getCertificateValidUntil()));
        assertEquals("certIdentifier42", firstRecovery.getCertificateIdentifier());

        RecoveryStatement secondRecovery = recoveryStatements[1];
        assertEquals("COVID-19", secondRecovery.getTarget().getValueSetEntry().getDisplay());
        assertEquals(LocalDate.of(2015, 10, 24), javaLocalDateOf(secondRecovery.getDateOfFirstPositiveTestResult()));
        assertEquals("country43", secondRecovery.getCountry());
        assertEquals("certificateIssuer43", secondRecovery.getCertificateIssuer());
        assertEquals(LocalDate.of(2015, 10, 25), javaLocalDateOf(secondRecovery.getCertificateValidFrom()));
        assertEquals(LocalDate.of(2015, 10, 26), javaLocalDateOf(secondRecovery.getCertificateValidUntil()));
        assertEquals("certIdentifier43", secondRecovery.getCertificateIdentifier());
    }

    private void assertTests(ehn.techiop.hcert.kotlin.data.Test[] tests) {
        assertEquals(2, tests.length);

        ehn.techiop.hcert.kotlin.data.Test firstTest = tests[0];
        assertEquals("COVID-19", firstTest.getTarget().getValueSetEntry().getDisplay());
        assertEquals("Nucleic acid amplification with probe detection", firstTest.getType().getValueSetEntry().getDisplay());
        assertEquals("nameNaa42", firstTest.getNameNaa());
        assertEquals("Abbott Rapid Diagnostics, Panbio COVID-19 Ag Test", Objects.requireNonNull(firstTest.getNameRat()).getValueSetEntry().getDisplay());
        assertEquals(LocalDateTime.of(2015, 10, 27, 12, 34, 56), javaLocalDateTimeOf(firstTest.getDateTimeSample()));
        assertNull(firstTest.getDateTimeResult());
        assertEquals("Not detected", firstTest.getResultPositive().getValueSetEntry().getDisplay());
        assertEquals("testFacility42", firstTest.getTestFacility());
        assertEquals("country42", firstTest.getCountry());
        assertEquals("certificateIssuer42", firstTest.getCertificateIssuer());
        assertEquals("certificateIdentifier42", firstTest.getCertificateIdentifier());

        ehn.techiop.hcert.kotlin.data.Test secondTest = tests[1];
        assertEquals("COVID-19", secondTest.getTarget().getValueSetEntry().getDisplay());
        assertEquals("Rapid immunoassay", secondTest.getType().getValueSetEntry().getDisplay());
        assertEquals("nameNaa43", secondTest.getNameNaa());
        assertEquals("AMEDA Labordiagnostik GmbH, AMP Rapid Test SARS-CoV-2 Ag", Objects.requireNonNull(secondTest.getNameRat()).getValueSetEntry().getDisplay());
        assertEquals(LocalDateTime.of(2015, 10, 27, 12, 34, 58), javaLocalDateTimeOf(secondTest.getDateTimeSample()));
        assertNull(secondTest.getDateTimeResult());
        assertEquals("Detected", secondTest.getResultPositive().getValueSetEntry().getDisplay());
        assertEquals("testFacility43", secondTest.getTestFacility());
        assertEquals("country43", secondTest.getCountry());
        assertEquals("certificateIssuer43", secondTest.getCertificateIssuer());
        assertEquals("certificateIdentifier43", secondTest.getCertificateIdentifier());
    }

}
