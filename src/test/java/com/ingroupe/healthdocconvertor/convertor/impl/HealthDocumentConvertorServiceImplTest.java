package com.ingroupe.healthdocconvertor.convertor.impl;

import com.ingroupe.healthdocconvertor.dto.DocumentRequest;
import com.ingroupe.healthdocconvertor.exception.Invalid2DDocPayloadException;
import com.ingroupe.healthdocconvertor.helper.impl.HealthDocumentConvertorHelperImpl;
import com.ingroupe.healthdocconvertor.service.impl.HealthDocumentConvertorServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class HealthDocumentConvertorServiceImplTest {

    @Autowired
    HealthDocumentConvertorServiceImpl healthDocumentConvertorService;

    @Autowired
    HealthDocumentConvertorHelperImpl hdcHelper;

    private final String PAYLOAD_EXAMPLE = "DC04FR0000011E6A1E6AL101FRL0THEOULE SUR MER%1DL1JEAN PIERRE%1DL230041962L3COVID-19%1DL4J07BX03%1DL5COMIRNATY PFIZER/BIONTECH%1DL6COMIRNATY PFIZER/BIONTECH%1DL71L82L901032021LACO%1FV5Y2ZQDNE5J3O7QKDQI2ZHKPEBLWTIFV6DS2AJXSVVU2MBJS3K6E5AR2CNNVBLE5ODIL2RFIJJUIM64HTMNPAB5TZYCW7RFVHW3J7IQ";


    private DocumentRequest buildDocumentRequest(DocumentRequest.DocumentType documentRequestParam1, String documentRequestParam2, DocumentRequest.DocumentType documentRequestParam3 ){
        DocumentRequest documentRequest = new DocumentRequest();
        documentRequest.setSource( documentRequestParam1);
        documentRequest.setChainEncoded( documentRequestParam2 );
        documentRequest.setDestination( documentRequestParam3 );
        return documentRequest;
    }

    @Test
    void givenJsonInputNullChainEncoded_shouldThrowException(){
        DocumentRequest documentRequest = buildDocumentRequest(DocumentRequest.DocumentType.DEUX_D_DOC, "", DocumentRequest.DocumentType.DGCA);
        assertThrows( Invalid2DDocPayloadException.class , () -> { healthDocumentConvertorService.decodeDocument(documentRequest) ;} ) ;
    }

    @Test
    void given2DDoc_shouldSubstringBeforeSeparator(){
        String test2DDoc = "firstname\u001Dsurname\u001Fsignature";
        String expected  = "77b76793eccd804006e21c18f989cf019535bdca7c1783a678a764f2d8e5b20e";
        assertEquals ( expected, healthDocumentConvertorService.getSignatureFrom2DDoc(test2DDoc) ) ;
    }


}
