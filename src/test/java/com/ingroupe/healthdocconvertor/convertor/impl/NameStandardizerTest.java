package com.ingroupe.healthdocconvertor.convertor.impl;

import com.ingroupe.healthdocconvertor.exception.CannotParse2DDocException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class NameStandardizerTest {

    @Test
    void givenNull_shouldThrowException() {
        CannotParse2DDocException exception =
            assertThrows(CannotParse2DDocException.class, () -> new NameStandardizer(null));
        assertEquals("A mandatory variable can not be found in the 2DDoc", exception.getMessage());
    }

    @Test
    void givenEmpty_shouldThrowException() {
        CannotParse2DDocException exception =
            assertThrows(CannotParse2DDocException.class, () -> new NameStandardizer(""));
        assertEquals("A mandatory variable can not be found in the 2DDoc", exception.getMessage());
    }

    @Test
    void givenRegularName_shouldReturnUpperCase() {
        NameStandardizer standardizer = new NameStandardizer("Dupont");
        assertEquals("DUPONT", standardizer.standardize());
    }

    @Test
    void givenNameWithAccent_shouldReturnNoAccentUpperCase() {
        NameStandardizer standardizer = new NameStandardizer("Côme");
        assertEquals("COME", standardizer.standardize());
    }

    @Test
    void givenSpace_shouldReturnOpeningAngleSquare() {
        NameStandardizer standardizer = new NameStandardizer("Elie Coptaire");
        assertEquals("ELIE<COPTAIRE", standardizer.standardize());
    }

    @Test
    void givenDashCharacter_shouldReturnEmpty() {
        NameStandardizer standardizer = new NameStandardizer("Lara-Masse");
        assertEquals("LARAMASSE", standardizer.standardize());
    }

    @Test
    void givenNonRegularName_shouldReturnNoAccentUpperCase() {
        NameStandardizer standardizer = new NameStandardizer("d'Červenková Panklová");
        assertEquals("DCERVENKOVA<PANKLOVA", standardizer.standardize());
    }

}
