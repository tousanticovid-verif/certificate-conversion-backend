package com.ingroupe.healthdocconvertor.convertor.impl;

import com.ingroupe.healthdocconvertor.exception.Invalid2DDocInputValueException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TestTypeTest {

    @Test
    void givenNull_shouldThrowException() {
        assertTestTypeThrowsException(null);
    }

    @Test
    void givenEmpty_shouldThrowException() {
        assertTestTypeThrowsException("");
    }

    @Test
    void givenNotMatchingTestType_shouldThrowException() {
        assertTestTypeThrowsException("foobar");
    }

    @Test
    void givenMatchingTestType_shouldNotThrowException() {
        assertDoesNotThrow(() -> TestType.forTwoDDocType("94500-6 PCR COVID"));
        assertDoesNotThrow(() -> TestType.forTwoDDocType("94309-2 PCR COVID"));
        assertDoesNotThrow(() -> TestType.forTwoDDocType("94845-5 PCR COVID"));
        assertDoesNotThrow(() -> TestType.forTwoDDocType("94558-4 Antigénique COVID"));
    }

    @Test
    void givenNotMatchingTestType_exceptionMessage() {
        Invalid2DDocInputValueException exception =
            assertThrows(Invalid2DDocInputValueException.class, () -> TestType.forTwoDDocType("foobar"));
        assertEquals("No corresponding type has been found for the test type", exception.getMessage());
    }

    private void assertTestTypeThrowsException(String input) {
        Invalid2DDocInputValueException exception =
            assertThrows(Invalid2DDocInputValueException.class, () -> TestType.forTwoDDocType(input));
        assertEquals("No corresponding type has been found for the test type", exception.getMessage());
    }

}
